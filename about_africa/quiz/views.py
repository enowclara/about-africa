from django.shortcuts import render
from django.http import HttpResponse
import random
from django.views.generic import TemplateView, ListView,CreateView


# def homepage(request):
#     return HttpResponse('homepage')

class QuizListView(ListView):
    template_name = "my_template.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

class CreateQuestionView(CreateView):
    prompt = ['question']
    template_name = "create_question.html"




